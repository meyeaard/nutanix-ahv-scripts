# Nutanix AHV Scripts

This repository holds my Nutanix specific scripts and occational documentation related to specific use cases for which they were created.

The Nutanix PowerShell Snapin CmdLets have some of the worst documentation and a complete lack of reference detail. I hope that even if these scripts are not pertinent to your needs they may provide some insight into at least how I use their CmdLets.

# Goals
1. **Safety:** Scripts should incorporate conflict testing and validation after any change.
1. **Simplify:** Script operational tasks that would be tedious and time consuming using acli/ncli thus reducing change risk.
1. **Delegatable:** Script tasks that would otherwise require access to acli/ncli so it can be delegated to those who are not allowed such access. 

# Use cases
- [Migrating VMs using Nutanix Move to a Nutanix AHV cluster where Guest OS lack support for VirtIO devices.](https://gitlab.com/meyeaard/nutanix-ahv-scripts/-/blob/master/documentation/AHV_Migrate%20Non-VirtIO%20Legacy%20GuestOS.md)

# Individual Scripts
- **[NTNX_ChnageDiskBusType.ps1](https://gitlab.com/meyeaard/nutanix-ahv-scripts/-/blob/master/scripts/NTNX_ChangeDiskBusType)**
  - Nutanix Snapin PowerShell script to reattach existing AHV VM vmDisks on one storage controller type to another.
  - Interchange vmDisks amongst: virtio scsi, virtio pci, sata, ide.
- **[NTNX_ChnageNicType.ps1](https://gitlab.com/meyeaard/nutanix-ahv-scripts/-/blob/master/scripts/NTNX_ChangeNicType)**
  - Nutanix Snapin PowerShell script to recrate an AHV VM vmNIC as as another type.
  - Interchange vmNIC model between: virtio and e1000.
- **[NTNX_NewNicCustomMAC.ps1](https://gitlab.com/meyeaard/nutanix-ahv-scripts/-/tree/master/scripts/NTNX_NewNicCustomMAC)**
  - Nutanix Snapin PowerShell script to add a new vmNIC to an AHV VM of a specified model and macAddress.
  - **WARNING** Could cause a MAC address conflict with other systems. The script will ensure the MAC does not conflict with any other vmNIC in the clsuter. It is the opperators responsibility to ensure there are no conflicts elsewhere.