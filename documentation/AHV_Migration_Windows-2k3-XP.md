# Purpose
The main compatibility issue with W2003/XP is that there are no device drivers available for the AHV/KVM paravirtualized SCSI controller typed ‘vioscsi’ which is the default controller type for AHV and the NTNX Move process.

The below details the VM preparation process to pre-inject KVM paravirtualized SCSI drivers for type 'viostor' and then the following migration and cutover procedure to convert disks connected as 'scsi' to 'pci'.

NOTE: the 'viostor' controller is a reduced SCSI command paravirtualized device. It is lightweight and performant for normal storage uses, it however does not support the extended SCSI command set needed for clustering such as windows failover clusters etc. If this is needed our option would be to upgrade OS to 2008+, or if 2003/XP is required we may be able to present those vmdisks as iSCSI via a NTNX Volume Group.

# Requirements
- Nutanix Powershell SnapIn
  - I can't find a direct download for this only from the Prism Central instance.
  - https:<PrismCentralFQDN>:9440/console/downloads/NutanixCmdlets.missed
  - Or Login to Prism Central, click your username in upper right corner, and select 'Download Cmdlets Installer'
- My scripts: NTNX_ChangeDiskBusType.ps1 and possibly NTNX_ChangeNicType.ps1 from this repo.
- And of course a VM with a CentOS/RHEL 5 (or possibly earlier) version that has no or bad VirtIO support.

# Prepare VM
- Ensure working local admin credentials for later offline login.
- Make note of TCP/IP configuration.
- Obtain devcon.exe utility.
    - Find Windows install or ServicePack ISO for your windows version.
      - These are still readily available from microsoft.com. 
    - Mount ISO.
    - Locate X:\SUPPORT\TOOLS\SUPPORT.CAB.
    - Extract devcon.exe
- Preinstall 'viostor' driver
  - I am using the stable KVM VirtIO windows drivers provided by the Fedora Project.
    - [https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/)
    - Download virtio drivers ISO from the above link.
      - I am using virtio-win-0.1.185.iso, but there may be a newer version available to you.
    - Upload this ISO to the Nutanix Image store for use post migration.
    - Mount the ISO and copy the viostor driver folder to local system.
    - Copy the devcon.exe file you sourced above into the viostor subfolder with your viostor.inf.
    - A simple BATCH file 'install.cmd' is provided in this repo to make the following easier. I however am not able to provide the drivers or devcon.exe utility due to licensing restrictions so you must aquire them yourself.
      - Just copy install.cmd into the same driver subfolder with devcon.exe and viostor.inf.
      - Run install.cmd from CMD or double-click.
      - Check Device Manager to ensure unreferenced devices were properly removed.
    - Run these commands to inject drivers, this will also create unreferenced devices.
      - These commands are noted in the viostor.inf file if you want to verify.
      - This will both installing the driver files and creating registry mappings for devices with these hardware ids.
      - `devcon install viostor.inf "PCI\VEN_1AF4&DEV_1001&SUBSYS_00021AF4&REV_00"`
      - `devcon install viostor.inf "PCI\VEN_1AF4&DEV_1042&SUBSYS_11001AF4&REV_01"`
    - Run these commands to remove the unreferenced devices.
      - `devcon remove viostor.inf "PCI\VEN_1AF4&DEV_1001&SUBSYS_00021AF4&REV_00"`
      - `devcon remove viostor.inf "PCI\VEN_1AF4&DEV_1042&SUBSYS_11001AF4&REV_01"`
- If you don't want to have an orphaned, disabled NIC configuration with your IP later on, change your network configuration to DHCP now.
  - If you skip this you will get warnings later when using the same IP on the Net-KVM adapter.
  - I don't expect this to cause issue, just an annoyance.
  - To clear this uplater requires diving into the registry which some are not comfortable with.

# Migrate VM
- Create NTNX Move migration plan.
  - When prompted for Preparation Mode.
  - Choose Manual.
  - No need to run the PowerShell snippets – they are not compatible and will fail.
  - Start migration plan.
  - Cutover VM to NTNX Cluster.
    - Cutover will leave VM powered on, BSOD boot looping due to inaccessible boot device.
    - Power off VM.
- Change vmdisks from bus type SCSI to bus type PCI to work with 2k3/XP VirtIO Drivers.
  - Open PowerShell console and CD to directory of NTNX_ChangeDiskBusType.ps1
  - Execute script to change bus type on all VirtIO 'scsi' virtual disks.
  - `NTNX_ChangeDiskBusType.ps1 -NtnxCluster <PrismElementFQDN> -vmName <VMNAME> -oldBus scsi -newBus pci`
  - Provide your local Prism Element credentials when prompted. (not Prism Central)  
  - Monitor script output to console and verify successful run.
- Power VM back up.
- Open VM console.
  - VM should now boot cleanly into Windows.
  - Mount current VirtIO driver ISO to VM.
  - Login with local admin creds.
  - Optional: Change VM Console resolution to 1024x768.
  - You will be prompted to install drivers for about 4 devices.
  - Have wizard install drivers from the CD.
  - There will be 1 device that we won’t have drivers for.
    - Named: 'SCSI Controller' Hardware ID: 'PCI\VEN_1AF4&DEV_1004…'
    - This is the VirtIO 'scsi' SCSI controller not supported on your OS.
    - Disable this device.
      - Open Device Manager.
      - Locate ‘SCSI Controller’ device with Hardware ID matching 'PCI\VEN_1AF4&DEV_1004…'
      - Open device Properties.
      - On General tab, change Device usage to Disabled.
  - Verify network device(s)
    - Set appropriate IP configuration.
    - You will likely recieve warning that IP is already used, ignore.
  - Reboot.
  - Verify OS is stable, verify no additional devices prompt for driver installation.
- Unmount CD-ROM ISO
- Perform application / service validation.
- Complete.

# Addendums
- If you have issues with Net-KVM support you may choose to convert the vmNic to e1000 type. See script NTNX_ChangeNicType.ps1
