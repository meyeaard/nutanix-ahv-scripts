# Nutanix AHV - Migrating Legacy OS Scripts

Scripts and documentation to facilitate migrating VMs with Legacy OS (no VirtIO device support) from any source supported by Nutanix Move to a Nutanix AHV cluster.

# Legacy OS Scope
My work here is centered around CentOS/RHEL 5 (Linux kernel 2.6.x) and Windows 2k3/XP.
However these processes / steps may also be relevant to other versions.
- [CentOS/RHEL 5](https://gitlab.com/meyeaard/nutanix-ahv-scripts/-/blob/master/documentation/AHV_Migration_CentOS-RHEL-5.md)
- [Windows 2003/XP x86/amd64](https://gitlab.com/meyeaard/nutanix-ahv-scripts/-/blob/master/documentation/AHV_Migration_Windows-2k3-XP.md)

# My Requirements
- Virtualized Device Selections:
  - Preference for VirtIO storage devices if possible (scsi / pci / net-kvm).
  - Avoid IDE vmDisks at all costs.
- Elimiate need for use of acli.
  - Allows OS System Engineers to perform migrations without requiring a Virtualization Engineer to assist.
  - Reduces risk of Engineer unfamilliar with acli causing accidental damage to cluster.
- Script, script, script.
  - Reduces risk further by automating change for OS System Engineer.
  - Perform potential confict testing and post change validation to reduce data loss potential.
    - If changing scsi-0 to pci-0, ensure first pci-0 isn't already allocated.
    - Verify pci-0 is properly created as clone of scsi-0 before destroying scsi-0.
  - Allow workarounds for common conflicts.
    - Allow override on matching disk device indexIds.
      - scsi-0 could become pci-1.
    - Allow controller change on only one disk instead of the default of all disks on oldBus.

