# Purpose
The main compatibility issue with the early 5.x versions of CentOS/RHEL is that there are no VirtIO kernel modules available for the AHV/KVM paravirtualized SCSI controller typed 'vioscsi' or 'viostor'. 'vioscsi' is the default full SCSI command set paravirtualized storage controller, 'viostor' is the reduced SCSI command set paravirtualized storage controller. Also there is a lack of support for the VirtIO network device which is default for AHV. Early support for these varies between CentOS/RHEL – I believe the earliest these devices were supported in CentOS was 5.4 although there are many, many bug fixes and improvements through the last 5.x release. If an upgrade is viable it would be beneficial to upgrade CentOS 5.x to at least the latest 5.x release – or better yet upgrade / replace the VM with one running a CentOS release under support!

The below details the VM preparation process I verified on CentOS 5.2; with migration / cutover procedure for non-VirtIO storage and networking devices.

NOTE: To gain support for storage and networking devices on AHV we will be using a 'sata' storage controller and an 'e1000' virtual network adapter. Know that using these devices instead of the more feature rich VirtIO variants will have inherent limitations in CPU overhead, supported number of vmDisks (6 on sata, 4 on IDE (3 really to have vCD-ROM)), and possible reduced storage / network throughput due to the fully virtualized devices and/or differences in device type (sata vs scsi) in general.

# Overview
The below proceedure may seem long, this is mostly due my verbose detailing. Here is a quick overview.
- Verify your VM fits within the limits of the legacy device types.
- Remove or disable legacy virtualization tools packages.
- Verify you have kernel modules to support the sata and e1000 devices.
- Modify configuration files to match devices after changes.
- Use Nutanix Move to migrate VM.
- Use provided scripts to change migrated VM devices to those supported.
- Power on and verify VM.

# Requirements
- Nutanix Powershell SnapIn
  - I can't find a direct download for this only from the Prism Central instance.
  - https:<PrismCentralFQDN>:9440/console/downloads/NutanixCmdlets.missed
  - Or Login to Prism Central, click your username in upper right corner, and select 'Download Cmdlets Installer'
- My scripts: NTNX_ChangeDiskBusType.ps1 and NTNX_ChangeNicType.ps1 from this repo.
- And of course a VM with a CentOS/RHEL 5 (or possibly earlier) version that has no or bad VirtIO support.

# Prepare VM
- Verify source VM has no more than 6 existing SCSI virtual disks. If the count is greater than 6 there may be creative solutions to this such as placing some low perf disks on IDE and the remainder on SATA, or utilizing NTNX Volume Groups via iSCSI. These scenarios however are custom and not detailed here.
- Ensure working local admin credentials (root or sudo account) 
- Remove or Disable vmware-tools / open-vm-tools
  - The tools packages may step on Guest configuration after migration, specifically /etc/fstab
  - `chkconfig --del vmware-tools` or `chkconfig --del open-vm-tools`
  - Look for broken symlinks under /etc that need to be fixed
    - On my test system: fstab, modulds.conf, modprobe.conf, updated.conf
    - All were broken references to files with '.BeforeVMwareToolsInstall' suffix.
    - Also existing were files with '.BeforeVMwareToolsInstall', '.AfterVMwareToolsInstall' or '~' suffix.
    - See which are correct and ensure these files are recreated as standard files.
- Verify kernel modules for SATA and Ethernet.
  - If these commands do not return device alias strings you will need to source correct modules.
    - AHV sata emulates ICH9, 6 channel sata: VEN_8086, DEV_2922
    - `modinfo ahci | grep -I v00008086d00002922`
    - AHV e1000 emulates Intel 82540EM 1GbE: VEN_8086, DEV_100E
    - `modinfo e1000 | grep -I v00008086d0000100E`
- Update initrd to include SATA drivers
  - Backup initrd: `cp /boot/initrd-$(uname -r).img /boot/initrd-$(uname -r).img.old`
  - Create new initrd: `mkinitrd --preload libata --preload ahci -f /boot/initrd-$(uname -r).img $(uname -r)`
  - Reboot and verify boot is successful.
- Update /etc/fstab
   - BLKID UUID
     - Blkid UUID is best practices for modern Linux fstab device mapping. However, testing this legacy migration process has shown that the volumes may not mount after the controller change even though UUID still match. This may be due to specific Linux distro tools compatibility with blkid used in testing or may be a deeper issue.
      - If blkid UUID is existing in your fstab, I recommend backing up your fstab and then modify changing to use /dev/device format. After a complete migration you can revert to the UUID backup version and verify.
  - If your /etc/fstab already uses /dev/device notation, you may need to update the values to use the device names of the NEW bus type.
  - PCI: devices will be /dev/vda,b,c,d,e,f...
  - SATA: devices will be /dev/sda,b,c,d,e,f (same as SCSI so no changes may be needed, but there is a 6 device limit.)
  - IDE: device will be /dev/hda,b,c,d
- LVM: In testing there was no effect in changing device names on LVM. However the native NTNX Move prep scripts temporarily disable lvm_metad so we should do that here as well.
  - Disable LVM metad if in use:
    - Edit /etc/lvm/lvm.conf
    - Find and make note of existing setting for 'use_lvmetad' if any.
    - If set, then change to use_lvmetad = 0
    - After migration is successful you can revert this.

# Migrate VM
- Create NTNX Move migration plan
  - When prompted for Preparation Mode, choose 'Manual'
  - No need to run the script snippets – they are not compatible with this version of Linux and will fail.
  - Start migration plan
- Cutover VM to NTNX Cluster
  - Cutover will leave VM powered on and in kernel panic due to inaccessible boot device.
  - Power off VM.
- Change vmdisks from bustype SCSI to bustype SATA to work with built in kernel modules.
  - Open PowerShell console and CD to directory of NTNX_ChangeDiskBusType.ps1
  - Execute script to change bus type on all VirtIO 'scsi' virtual disks.
    - `NTNX_ChangeDiskBusType.ps1 -NtnxCluster <PrismElementFQDN> -vmName <VMNAME> -oldBus scsi -newBus sata`
    - Requires Prism Element credentials for this cluster. (not Prism Central) 
    - Monitor script output to console and verify successful run.
- Change vNIC from VirtIO to e1000 to work with built in kernel modules
  - From Prism obtain the MAC address of the interface you need to convert.
  - Open PowerShell console and CD to directory of NTNX_ChangeNicType.ps1
  - Execute script to recreate vmNIC as e1000 retaining MAC address.
    - `NTNX_ChangeNicType.ps1 -NtnxCluster <PrismElementFQDN> -vmName <VMNAME> -macAddress '11:22:33:44:55:66'`
      - Requires Prism Element credentials for this cluster. (not Prism Central) 
    - Monitor script output to console and verify successful run.
- Power VM back up
  - Open VM console
  - If prep steps were done properly you *should* have successful boot into Linux
    - If not you may be able to fix issues by interrupting GRUP and postpending init=/bin/bash to vmkernel line.
    - Even if / shows mounted rw, I've found issues with rw on config files without first `mount -o remount rw /`
    - Usual items missed are:
      - Forgetting to convert disk devices to sata or vmNic to e1000.
      - Using incorrect device names / ids in fstab.
  - Check 'dmesg' for module load / device failures.
  - Verify all /etc/fstab volumes mounted successfully.
    - If you had UUID mapping before, revert to it now
    - Reboot again to verify
  - If you had LVM lvm_metad enabled before you can reenable that now.
  - Verify network interfaces are UP and have IP allocated. Verify default route.
    - `/sbin/ip a`
    - `/sbin/ip r`
    - If your system uses the new device naming convention you may need to update the network configuration to match the new device name. However, if your distro is new enough for that it should support VirtIO and you should be using that!
  - Perform application / service validation.
- Complete.

