Param(
    [Parameter(HelpMessage='Nutanix Prism Element FQDN.',Mandatory=$true)]$NtnxCluster,
    [Parameter(HelpMessage='VM Name',Mandatory=$true)]$vmName,
    [Parameter(HelpMessage='Credetntial',Mandatory=$false)][pscredential]$Cred,
    [Parameter(HelpMessage='Current vmNIC mac address',Mandatory=$true)]$macAddress,
    [Parameter(HelpMessage='New vmNIC type')]
        [ValidateSet('virtio','e1000')]$newNicType = 'e1000',
    [Parameter(HelpMessage='User Confirmation Required')]$Confirm = $true
)

<#
.SYNOPSIS
    Change NTNX AHV VM virtual NIC controller type.

.DESCRIPTION
    Script will take existing NTNX AHV VM virtual NIC of one controller type, 
    remove it and create a new virtual NIC of an alternate controller type but with matching MAC.
    
    ?This operation requires that the VM is in a powered off state.

    Ensure that the VM Guest OS has appropriate drivers preinstalled or the device will not be available after change.
    'virtio' vmNic device VEN_1af4,DEV_1002
    'e1000' vmNic device VEN_8086,DEV_100e
    
.NOTES
    Author: Aaron Meyer

.PARAMETER NtnxCluster (Required)
    Nutanix Cluster Prism Element (PE) instance FQDN managing VM.

.PARAMETER vmName (Required)
    Name of the VM to action on as displayed in the Prism VM inventory.

.PARAMETER Cred
    PowerShell credential object containing access credentials for Prism Element.

.PARAMETER macAddress
    MAC address of existing vmNIC to recreate as alternate controller type.
    Example: -macAddress '50:6b:8d:ca:1a:71'

.PARAMETER newNicType
    VM NIC controller type to change TO, Default=e1000
    Options: e1000, virtio

.PARAMETER Confirm
    If $True (default) require user confirmation before making changes.

.EXAMPLE
    Change VM 'MyVMName' from virtio to e1000 vmNIC. Script will prompt for credentials.
    & '.\NTNX_ChangeNicType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'MyVMName' -macAddress '11:22:33:44:55:66'

    Change VM 'TestVM2' from e100 to virtio NIC using already defined credentials.
    Useful if OS was later upgraded and now supports VirtIO devices.
    & '.\NTNX_ChangeNicType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM2' -Cred $MyCreds -macAddress '11:22:33:44:55:66'-newNicType 'virtio'
#>

$LoadedSnapins = Get-PSSnapin -Name NutanixCmdletsPSSnapin -ErrorAction SilentlyContinue
if (-not $LoadedSnapins) {   
    Try{
        Add-PsSnapin NutanixCmdletsPSSnapin -ErrorAction Stop
        }
    Catch{
        Write-Host 'Nutanix PSSnapin could not be loaded'
        Write-Host 'Ensure Nutanis PowerShell Cmdlets is installed'
        Exit 1
    }
}

# Get creds if we weren't provided any.
If (! $Cred){
    $Cred = Get-Credential -Message "Admin credentials for NTNX Cluster $NtnxCluster"
}

Write-Host "Connecting to NTNX Cluster: $NtnxCluster... " -NoNewline
$NtnxConnection = Connect-NTNXCluster -Server $NtnxCluster -UserName $Cred.UserName -Password $Cred.Password -AcceptInvalidSSLCerts -ForcedConnection
If (! $NtnxConnection.IsConnected){
    Write-Host -ForegroundColor:Yellow ("FAILED`nUnable to connect to NTNX Cluster "+$NtnxCluster)
    Write-Host -ForegroundColor:Yellow $Error
    Exit 1
} Else{
    Write-Host -ForegroundColor:Green "SUCCESS"
}

# Get VM Info
$vmInfo = $null
$vmInfo = Get-NTNXVM | Where-Object {$_.vmName -eq $vmName}
If (! $vmInfo){
    Write-Host -ForegroundColor:Yellow 'Unable to find VM with specified name.'
    Exit 1
} ElseIf ($vmInfo.Count -gt 1){
    Write-Host -ForegroundColor:Yellow 'Multiple VMs returned! Do not use wildcard and ensure vmName is unique in cluster!'
    Exit 1
} ElseIf ($vmInfo.powerState -ne 'off'){
    Write-Host -ForegroundColor:Yellow 'VM was found but is currently powered on. Power off VM and try again.'
    Exit 1
} Else {
    Write-Host -ForegroundColor:Green 'Located VM and verified powered off state.'
}

# Get existing NIC
$oldNic = $null
$oldNic = Get-NTNXVMNIC -Vmid $vmInfo.vmId | Where-Object {$_.macAddress -ieq $macAddress}
If ($null -eq $oldNic){
    Write-Host -ForegroundColor:Red ('Unable to find vmNIC with MAC:'+$macAddress+' on VM:'+$vmInfo.vmName+' with vmID:'+$vmInfo.vmId)
    Exit 1
}
# Create new NIC spec
$newNic = New-NTNXObject -Name VMNicSpecDTO
If ($newNicType -eq 'e1000'){
    $newNic.model = 'e1000'
} Else {
    # VirtIO is default, set value ''
    $newNic.model = ''
}

$newNic.macAddress = $macAddress
$newNic.networkUuid = $oldNic.networkUuid
$newNic.isConnected = $oldNic.isConnected

Write-Host '##################################################' -ForegroundColor:Blue
Write-Host '# VM NIC Change detail' -ForegroundColor:Blue
Write-Host '##################################################' -ForegroundColor:Yellow
Write-Host '# Existing VM NIC Configuration:' -ForegroundColor:Yellow
Write-Host '#-------------------------------------------------' -ForegroundColor:Yellow
Write-host ('# macAddress:'+$oldNic.macAddress) -ForegroundColor:Yellow
Write-host ('# model:'+$oldNic.model) -ForegroundColor:Yellow
Write-host ('# networkUuid:'+$oldNic.networkUuid) -ForegroundColor:Yellow
Write-host ('# isConnected:'+$oldNic.isConnected) -ForegroundColor:Yellow
Write-Host '##################################################' -ForegroundColor:Green
Write-host '# New VM NIC Configuration:' -ForegroundColor:Green
Write-Host '#-------------------------------------------------' -ForegroundColor:Green
Write-host ('# macAddress:'+$newNic.macAddress) -ForegroundColor:Green
Write-host ('# model:'+$newNic.model) -ForegroundColor:Green
Write-host ('# networkUuid:'+$newNic.networkUuid) -ForegroundColor:Green
Write-host ('# isConnected:'+$newNic.isConnected) -ForegroundColor:Green
Write-Host '##################################################' -ForegroundColor:Green

# Confirm with user before making changes.
If ($Confirm){
    Write-Host "Do you approve / confirm making changes? (Y/N):" -NoNewline
    Do {
        $in = Read-Host
    } Until ($in -iin ('Y','N'))
    If ($in -ieq 'N'){ Exit }
}

# Clear any existing error history
$Error.Clear()

Write-Host ('Processing VM NIC for MAC:'+$macAddress)
Write-Host "`tRemoving existing VM NIC:" -NoNewline
# Remove source VM NIC so we don't have a MAC collision.
$taskRemove = $null
$taskRemove = Remove-NTNXVMNIC -Vmid $vmInfo.vmId -Nicid $macAddress
If ($taskRemove.taskUuid){
    $taskRemoveStatus = $null
    $jobStatusCounter = 0
    Do {
        if ($jobStatusCounter -ge 1){ Start-Sleep -Seconds 1 }
        $jobStatusCounter++
        $taskRemoveStatus = Get-NTNXTask -Taskid $taskRemove.taskUuid
    } Until ($taskRemoveStatus.progressStatus -in ('Failed','Succeeded'))
    Switch ($taskRemoveStatus.progressStatus){
        'Failed' {
            Write-Host -ForegroundColor:Red ("FAILED`n`tUnable to remove source NIC "+$oldNic.nicUuid+' manual remediation required.')
        }
        'Succeeded' {
            Write-Host -ForegroundColor:Green ('SUCCESS')
        }
    }                       

    # ONLY run the following if there was no error removing the old NIC!
    If (!$Error -and ($taskRemoveStatus.progressStatus -eq 'Succeeded')){
        Write-Host "`tAdding new VM NIC:" -NoNewline
        $taskAdd = Add-NTNXVMNIC -Vmid $vmInfo.vmId -SpecList $newNic -ErrorAction:Stop
        $taskAddStatus = $null
        $jobStatusCounter = 0
        Do {
            If ($jobStatusCounter -gt 0){ Start-Sleep -Seconds 1}
            $jobStatusCounter++
            $taskAddStatus = Get-NTNXTask -Taskid $taskAdd.taskUuid
        } Until ($taskAddStatus.progressStatus -in ('Failed','Succeeded'))
        Switch ($taskAddStatus.progressStatus){
            'Failed' {
                Write-Host -ForegroundColor:Red "FAILED`n`tUnable to add new VM NIC, manual remediation required."
            }
            'Succeeded' {
                Write-Host -ForegroundColor:Green 'SUCCESS'
                $verifyNic = $false
                $verifyNic = Get-NTNXVMNic -Vmid $vmInfo.vmId | Where-Object {($_.macAddress -eq $macAddress) -and ($_.model -eq $newNic.model) -and ($_.networkUuid -eq $newNic.networkUuid)}
                If (! $verifyNic){ 
                    Write-Host -ForegroundColor:Red ("`tUnable to verify creation of new NIC : macAddress, model, networkUuid.`n"+$verifyNic)
                } ElseIf ($verifyNic.Count -gt 1){
                    Write-Host -ForegroundColor:Red "`tSomehow we have more than 1 VM NICs with the same MAC! Remove all but one of them ASAP!"
                } Else {
                    Write-Host -ForegroundColor:Green "`tVerfied NIC: macAddress, model, networkUuid`n`n"
                    Write-Host '##################################################' -ForegroundColor:Green
                    Write-host '# Verified VM NIC Configuration:' -ForegroundColor:Green
                    Write-Host '#-------------------------------------------------' -ForegroundColor:Green
                    Write-host ('# macAddress:'+$verifyNic.macAddress) -ForegroundColor:Green
                    Write-host ('# model:'+$verifyNic.model) -ForegroundColor:Green
                    Write-host ('# networkUuid:'+$verifyNic.networkUuid) -ForegroundColor:Green
                    Write-host ('# isConnected:'+$verifyNic.isConnected) -ForegroundColor:Green
                    Write-Host '##################################################' -ForegroundColor:Green
                }
            }
        }
    } Else {
        Write-Host -ForegroundColor:red "Errors occurred when removing original VM NIC."
        Write-Host -ForegroundColor:red "Check state of VM configuration and try again."
    }
} Else {
    Write-Host -ForegroundColor:red "Unknown issue calling Remove-NTNXVMNIC. No task UUID was returned."
    Write-Host '$taskRemove'
    $taskRemove
    Write-Host '$Error'
    $Error
}
