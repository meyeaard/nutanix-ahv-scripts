# Synopsis
- Change NTNX AHV VM virtual NIC controller type.
# Description
- Script will take existing NTNX AHV VM virtual NIC of one controller type, remove it and create a new virtual NIC of an alternate controller type but with matching MAC.
- This script requires that the VM is in a powered off state.
- Ensure that the VM Guest OS has appropriate drivers preinstalled or the device will not be available after change.
  - 'virtio' vmNic device VEN_1af4,DEV_1002
  - 'e1000' vmNic device VEN_8086,DEV_100e
# Notes
- Author: Aaron Meyer
# Parameters
- **-NtnxCluster** (Required) [string]
  - Nutanix Cluster Prism Element (PE) instance FQDN or IP that is managing the VM.
  - -NtnxCluster 'PrismElement.example.com'
  - -NtnxCluster '192.168.52.34'
- **-vmName** (Required) [string]
  - Name of the VM to action on as displayed in Prism VM inventory.
- **-Cred** [PSCredential]
  - PowerShell credential object containing access credentials for Prism Element.
- **-macAddress** [string]
  - MAC address of existing vmNIC to recreate as alternate controller type.
  - Example: -macAddress '50:6b:8d:ca:1a:71'
- **-newNicType** [string]
  - VM NIC controller type to change TO, Default=e1000
  - Options: e1000, virtio
- **-Confirm** [boolean]
   - If $true (default) require user confirmation before making changes.
   - If $false there will be no user confirmation before making changes.
# Examples
- Change VM 'MyVMName' from virtio to e1000 vmNIC. Script will prompt for credentials.
  - `& '.\NTNX_ChangeNicType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'MyVMName' -macAddress '11:22:33:44:55:66'`
- Change VM 'TestVM2' from e100 to virtio NIC using already defined credentials. Useful if OS was later upgraded and now supports VirtIO devices.
  - `& '.\NTNX_ChangeNicType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM2' -Cred $MyCreds -macAddress '11:22:33:44:55:66'-newNicType 'virtio'`
