Param(
    [Parameter(HelpMessage='Nutanix Prism Element FQDN.',Mandatory=$true)]$NtnxCluster,
    [Parameter(HelpMessage='VM Name',Mandatory=$true)]$vmName,
    [Parameter(HelpMessage='Name of Virtual Network to assign to new vmNic')]$NtnxNetworkname,
    [Parameter(HelpMessage='Credetntial',Mandatory=$false)][pscredential]$Cred,
    [Parameter(HelpMessage='Current vmNIC mac address',Mandatory=$true)]$macAddress,
    [Parameter(HelpMessage='New vmNIC type')]
        [ValidateSet('virtio','e1000')]$newNicType = 'virtio',
    [Parameter(HelpMessage='Connection state of new vmNIC')][bool]$isConnected = $true,
    [Parameter(HelpMessage='User Confirmation Required')]$Confirm = $true
)

<#
.SYNOPSIS
    NTNX AHV VM Add virtual NIC with custom MAC

.DESCRIPTION
    Script will add a new virtual NIC to an NTNX AHV VM of specified type and macAddress.
    
    Ensure that the VM Guest OS has appropriate drivers preinstalled or the device will not be available after change.
    'virtio' vmNic device VEN_1af4,DEV_1002
    'e1000' vmNic device VEN_8086,DEV_100e
    
.NOTES
    Author: Aaron Meyer

.PARAMETER NtnxCluster (Required) [string]
    Nutanix Cluster Prism Element (PE) instance FQDN managing VM.

.PARAMETER vmName (Required) [string]
    Name of the VM to action on as displayed in the Prism VM inventory.

.PARAMETER NtnxNetworkName (Required) [string]
    Name of the Nutanix VM network to assign to the new vmNIC.

.PARAMETER Cred [pscredential]
    PowerShell credential object containing access credentials for Prism Element.

.PARAMETER macAddress [string]
    MAC address of existing vmNIC to recreate as alternate controller type.
    Example: -macAddress '50:6b:8d:ca:1a:71'

.PARAMETER newNicType [string]
    VM NIC controller type to change TO, Default=virtio
    Options: e1000, virtio

.PARAMETER Confirm [bool]
    If $True (default) require user confirmation before making changes.

.EXAMPLE
    Add NIC to VM 'MyVMName' of default type virtio. Script will prompt for credentials.
    & '.\NTNX_NewNicCustomMac.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'MyVMName' -macAddress '11:22:33:44:55:66'

    Add NIC to VM 'TestVM2' of type e1000 using already defined credentials.
    Useful if OS was later upgraded and now supports VirtIO devices.
    & '.\NTNX_NewNicCustomMac.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM2' -Cred $MyCreds -macAddress '11:22:33:44:55:66'-newNicType 'e1000'
#>

$LoadedSnapins = Get-PSSnapin -Name NutanixCmdletsPSSnapin -ErrorAction SilentlyContinue
if (-not $LoadedSnapins) {   
    Try{
        Add-PsSnapin NutanixCmdletsPSSnapin -ErrorAction Stop
        }
    Catch{
        Write-Host 'Nutanix PSSnapin could not be loaded'
        Write-Host 'Ensure Nutanis PowerShell Cmdlets is installed'
        Exit 1
    }
}

# Get creds if we weren't provided any.
If (! $Cred){
    $Cred = Get-Credential -Message "Admin credentials for NTNX Cluster $NtnxCluster"
}

Write-Host "Connecting to NTNX Cluster: $NtnxCluster... " -NoNewline
$NtnxConnection = Connect-NTNXCluster -Server $NtnxCluster -UserName $Cred.UserName -Password $Cred.Password -AcceptInvalidSSLCerts -ForcedConnection
If (! $NtnxConnection.IsConnected){
    Write-Host -ForegroundColor:Yellow ("FAILED`nUnable to connect to NTNX Cluster "+$NtnxCluster)
    Write-Host -ForegroundColor:Yellow $Error
    Exit 1
} Else{
    Write-Host -ForegroundColor:Green "SUCCESS"
}

# Get VM Info
$vmInfo = $null
$vmInfo = Get-NTNXVM | Where-Object {$_.vmName -ieq $vmName}
# Verify VM Info
If (! $vmInfo){
    Write-Host -ForegroundColor:Yellow 'Unable to find VM with specified name.'
    Exit 1
} ElseIf ($vmInfo.Count -gt 1){
    Write-Host -ForegroundColor:Yellow 'Multiple VMs returned! Do not use wildcard and ensure vmName is unique in cluster!'
    Exit 1
} ElseIf ($vmInfo.powerState -ne 'off'){
    Write-Host -ForegroundColor:Yellow 'VM was found but is currently powered on. Power off VM and try again.'
    Exit 1
} Else {
    Write-Host -ForegroundColor:Green 'Located VM and verified powered off state.'
}

# Get VM network Info
$vmNetwork = Get-NTNXNetwork | Where-Object { $_.name -ieq $NtnxNetworkname }
# Verify VM network info
If (! $vmNetwork){
    Write-Host -ForegroundColor:Yellow 'Unable to find VM Network with specified name.'
    Write-Host -ForegroundColor:Yellow 'Check VM network name and don''t typo'
    Exit 1
} ElseIf ($vmNetwork.Count -gt 1){
    Write-Host -ForegroundColor:Yellow 'Multiple VM Networks returned! This should not happen!'
    Exit 1
} Else {
    Write-Host -ForegroundColor:Green ('Located VM Network:'+$vmNetwork.name+' VLAN:'+$vmNetwork.vlanId+' uuid:'+$vmNetwork.uuid)
}

# Try to verify that this MAC is unique - at least inside NTNX
Write-Host 'Duplicate MAC addresses are dangerous!' -ForegroundColor:Yellow
Write-Host "`tStarting remedial check to at least verfy MAC is unique to this PrismElement" -ForegroundColor:Yellow
Write-Host "`tQuerying Prism for All non CVM VM NICs: " -ForegroundColor:Yellow -NoNewline

# Get ALL VMs, except CVM
# NTNX PowerShell makes this difficult... Making a hash table so we can reverse lookup if duplicate found.
# Really NTNX, No parent, child relationship in these objects! No uuid on VM NIC?! No reference to MAC (NIC ID) on VM?!
$macTable = @()
$allVMs = Get-NTNXVM | Where-Object {$_.controllerVM -eq $false}
# Itterate through all VMs, lookup their NIC(s) and populate macTable.
Foreach ($v in $allVMs){
    $vmNICs = @()
    $vmNICs += Get-NTNXVMNIC -Vmid $v.vmId
    ForEach ($n in $vmNICs){
        $macTableRow = '' | Select-Object vmName, macAddress
        $macTableRow.vmName = $v.vmName
        $macTableRow.macAddress = $n.macAddress
        $macTable += $macTableRow
    }
}
# Verify data returned
If ($macTable.Count -eq 0){
    Write-Host -ForegroundColor:Red "`nSomething is wrong. No VM NICs returned."
    Exit 1
} Else {
    Write-Host -ForegroundColor:Green (($macTable.Count).ToString()+' VM NICs found.')
}
# Look for duplicate in returned macAddress list
$dupMACs = $macTable | Where-Object {$_.macAddress -eq $macAddress}
If ($dupMACs.Count -ne 0){
    # Find what VM has the macAddress now.
    Write-Host ("`tDANGER EXIT NOW: "+$macAddress+" is already used by VM(s):"+($dupMACs.vmName) -join(',')) -ForegroundColor:red
    Exit 1
} Else {
    Write-Host ("`tNo duplicates found for MAC:"+$macAddress)
}

# Create new NIC spec
$newNic = New-NTNXObject -Name VMNicSpecDTO
If ($newNicType -eq 'e1000'){
    $newNic.model = 'e1000'
} Else {
    # VirtIO is AHV default, set value ''
    $newNic.model = ''
}
$newNic.networkUuid = $vmNetwork.uuid
$newNic.macAddress = $macAddress
$newNic.isConnected = $isConnected

Write-Host '##################################################' -ForegroundColor:Blue
Write-Host '# VM NIC Change detail' -ForegroundColor:Blue
Write-Host '##################################################' -ForegroundColor:Green
Write-host '# New VM NIC Configuration:' -ForegroundColor:Green
Write-Host '#-------------------------------------------------' -ForegroundColor:Green
Write-host ('# macAddress:'+$newNic.macAddress) -ForegroundColor:Green
Write-host ('# model:'+$newNic.model) -ForegroundColor:Green
Write-host ('# NtnxNetworkName:'+$vmNetwork.name) -ForegroundColor:Green
Write-host ('# networkUuid:'+$vmNetwork.uuid) -ForegroundColor:Green
Write-host ('# isConnected:'+$newNic.isConnected) -ForegroundColor:Green
Write-Host '##################################################' -ForegroundColor:Green

# Confirm with user before making changes.
If ($Confirm){
    Write-Host "Do you approve / confirm making changes? (Y/N):" -NoNewline
    Do {
        $in = Read-Host
    } Until ($in -iin ('Y','N'))
    If ($in -ieq 'N'){ Exit }
}

Write-Host ('Processing VM NIC for MAC:'+$macAddress)
Write-Host "`tAdding new VM NIC:" -NoNewline
# Add vmNIC
$taskAdd = Add-NTNXVMNIC -Vmid $vmInfo.vmId -SpecList $newNic -ErrorAction:Stop
# Wait for vmNIC Add job to complete
$taskAddStatus = $null
$jobStatusCounter = 0
Do {
    If ($jobStatusCounter -gt 0){ Start-Sleep -Seconds 1}
    $jobStatusCounter++
    $taskAddStatus = Get-NTNXTask -Taskid $taskAdd.taskUuid
} Until ($taskAddStatus.progressStatus -in ('Failed','Succeeded'))
Switch ($taskAddStatus.progressStatus){
    'Failed' {
        Write-Host -ForegroundColor:Red "FAILED`n`tUnable to add new VM NIC, manual remediation required."
    }
    'Succeeded' {
        Write-Host -ForegroundColor:Green 'SUCCESS'
        $verifyNic = $false
        $verifyNic = Get-NTNXVMNic -Vmid $vmInfo.vmId | Where-Object {($_.macAddress -eq $macAddress) -and ($_.model -eq $newNic.model) -and ($_.networkUuid -eq $newNic.networkUuid)}
        If (! $verifyNic){ 
            Write-Host -ForegroundColor:Red ("`tUnable to verify creation of new NIC : macAddress, model, networkUuid.`n"+$verifyNic)
        } ElseIf ($verifyNic.Count -gt 1){
            Write-Host -ForegroundColor:Red "`tSomehow we have more than 1 VM NICs with the same MAC! Remove all but one of them ASAP!"
        } Else {
            Write-Host -ForegroundColor:Green "`tVerfied NIC: macAddress, model, networkUuid`n`n"
            Write-Host '##################################################' -ForegroundColor:Green
            Write-host '# Verified VM NIC Configuration:' -ForegroundColor:Green
            Write-Host '#-------------------------------------------------' -ForegroundColor:Green
            Write-host ('# macAddress:'+$verifyNic.macAddress) -ForegroundColor:Green
            Write-host ('# model:'+$verifyNic.model) -ForegroundColor:Green
            Write-host ('# networkUuid:'+$verifyNic.networkUuid) -ForegroundColor:Green
            Write-host ('# isConnected:'+$verifyNic.isConnected) -ForegroundColor:Green
            Write-Host '##################################################' -ForegroundColor:Green
        }
    }
}