# Synopsis
NTNX AHV VM Add virtual NIC with custom MAC

# Description
Script will add a new virtual NIC to an NTNX AHV VM of specified type and macAddress.

- Ensure that the VM Guest OS has appropriate drivers preinstalled or the device will not be available after change.
  - 'virtio' vmNic device VEN_1af4,DEV_1002
  - 'e1000' vmNic device VEN_8086,DEV_100e
    
# Notes
Author: Aaron Meyer

# Parameters
- **-NtnxCluster** (Required) [string]
  - Nutanix Cluster Prism Element (PE) instance FQDN managing VM.
- **-vmName** (Required) [string]
  - Name of the VM to action on as displayed in the Prism VM inventory.
- **-NtnxNetworkName** (Required) [string]
  - Name of the Nutanix VM network to assign to the new vmNIC.
- **-Cred** [pscredential]
  - PowerShell credential object containing access credentials for Prism Element.
- **-macAddress** [string]
  - MAC address of existing vmNIC to recreate as alternate controller type.
  - Example: -macAddress '50:6b:8d:ca:1a:71'
- **-newNicType** [string]
  - VM NIC controller type to change TO, Default=virtio
  - Options: e1000, virtio
- **-Confirm** [bool]
  - If $True (default) require user confirmation before making changes.

# Examples
- Add NIC to VM 'MyVMName' of default type virtio. Script will prompt for credentials.
    - `& '.\NTNX_NewNicCustomMac.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'MyVMName' -macAddress '11:22:33:44:55:66'`
- Add NIC to VM 'TestVM2' of type e1000 using already defined credentials. Useful if OS was later upgraded and now supports VirtIO devices.
    - `& '.\NTNX_NewNicCustomMac.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM2' -Cred $MyCreds -macAddress '11:22:33:44:55:66'-newNicType 'e1000'`
