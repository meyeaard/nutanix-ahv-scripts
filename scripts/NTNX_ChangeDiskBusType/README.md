# Synopsis
**Change NTNX AHV VM virtual disk storage controller bus type.**

# Description
Script will take existing NTNX AHV VM virtual disks of one controller bus type, clone them to new virtual disks of a different controller bus type with the same device index, and finally cleanup, removing original virtual disks.

This operation requires that the VM is in a powered off state.

Ensure that the VM Guest OS has appropriate drivers preinstalled or it will not boot after change.
#### /etc/fstab
Linux systems may need special care in /etc/fstab to ensure block devices mount properly.
- Using blkid UUIDs to reference block devices in fstab is the best practice. However note that in my testing I found that even though the device UUID did not change when changing the vmDisk to another controller type the volume would not mount. You may be smarter than I and know the cause / solution.
    - My workaround is to change fstab entries back to /dev/devicename notation, make the controller change, and then reconfigure UUID mapping.
- Any block devices in fstab using /dev/devicename notation may need to be modified to match the block device names after reboot.
  - scsi and sata both use sd devicename prefix. i.e. /dev/sda, /dev/sdb, etc.
    - The big difference there though is that AHV only supports 6 vmDisks on the sata bus.
    - The maximum number of scsi vmDisks to a VM is 256.
  - ide uses the hd devicename prefix. i.e. /dev/hda, /dev/hdb, etc.
    - Note the limitation of a maximum of 4 vmDisks on the ide bus.
    - Also, ideally you will limit to at most 3 vmDisk block devices so you can still utilize a virtual CD-ROM.
  - pci uses the vd devicename prefix. i.e. /dev/vda, /dev/vdb, etc.
    - The maximum number of pci vmDisks to a VM is 7.

# Notes
Author: Aaron Meyer

# Script Parameters
- **-NtnxCluster** (Required)
  - Nutanix Cluster Prism Element (PE) instance FQDN managing VM.
- **-vmName** (Required)
  - Name of the VM to action on as displayed in the Prism VM inventory.
- **-Cred**
  - PowerShell credential object containing access credentials for Prism Element.
- **-oldBus**
  - VM disk storage controller type to change FROM. Default=scsi
  - Options: scsi, ide, sata, pci
- **-newBus**
  - VM disk storage controller type to change TO, Default=pci
  - Options: scsi, ide, sata, pci
- **-vmDiskId**
  - Specific vmDisk to change in bus-id notiation, Default=$null
  - If unset, the script will change ALL vmDisk(s) on oldBus type to newBus type. If you only want to modify a singular vmDisk use this parameter to specify the disk. This is useful if you only wish to change one of many vmDisks to for example place some vmDisks on sata and others on ide when number of total vmDisks is > 6 and Guest OS lacks support for scsi and pci.
  - Options: scsi-0, sata-1, ide-3, etc.
- **-indexOverride**
  - Allocate VM disk bus inded as next-available instead of default which will match the source disk index.
  - i.e. scsi-0 could become pci-2 (sda to vdc vs. vda)
  - Most likely used when chaning to IDE bus type and CD-ROM owns ide-0. Alternatively, remove CD-ROM, make change, re-add CD-ROM.
  - WARNING: This may cause Guest OS to not properly mount block devices, UEFI boot options, etc. Use with caution. Ensure Linux fstab entires are referencing blkid and not device name. Have OS recovery media avialable in case manual intervention in guest OS is required.
- **-Confirm**
  - If $true (default) require user confirmation before making changes.
  - If $false make changes without any user prompts.
- **-Examples**
  - Change VM 'MyVMName' from (default oldBus) scsi to (default newBus) pci storage bus. Script will prompt for credentials. 
    - `& '.\NTNX_ChangeDiskBusType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'MyVMName'`
  - Change VM 'TestVM1' from (default oldBus) scsi to sata storage bus using defined credentials.
    - `& '.\NTNX_ChangeDiskBusType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM1' -Cred $MyCreds -newBus 'sata'`
  - Change VM 'TestVM2 from sata to scsi using defined credentials. Useful if you upgrade Guest OS and it now supports VirtIO devices.
    - `& '.\NTNX_ChangeDiskBusType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM2' -Cred $MyCreds -oldBus 'sata' -newBus 'scsi'`
  - Only change VM 'TestVM3' vmDisk on scsi-0 to sata.
    - `& '.\NTNX_ChangeDiskBusType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM3' -Cred $MyCreds -vmDiskId 'scsi-0' -oldBus 'sata' -newBus 'scsi'`  