@ECHO OFF
ECHO.
ECHO Adding VirtIO BLK drivers.
devcon.exe install viostor.inf "PCI\VEN_1AF4&DEV_1001&SUBSYS_00021AF4&REV_00"
devcon.exe install viostor.inf "PCI\VEN_1AF4&DEV_1042&SUBSYS_11001AF4&REV_01"
ECHO.
ECHO Removing unreferenced devices created by above process.
devcon.exe remove viostor.inf "PCI\VEN_1AF4&DEV_1001&SUBSYS_00021AF4&REV_00"
devcon.exe remove viostor.inf "PCI\VEN_1AF4&DEV_1042&SUBSYS_11001AF4&REV_01"
ECHO.
ECHO Verify above.
PAUSE