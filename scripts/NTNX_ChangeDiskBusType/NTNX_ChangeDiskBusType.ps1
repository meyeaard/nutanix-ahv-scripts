Param(
    [Parameter(HelpMessage='Nutanix Prism Element FQDN.',Mandatory=$true)]$NtnxCluster,
    [Parameter(HelpMessage='VM Name',Mandatory=$true)]$vmName,
    [Parameter(HelpMessage='Credetntial',Mandatory=$false)][pscredential]$Cred,
    [Parameter(HelpMessage='Current vmDisk bus type')]
        [ValidateSet('scsi','pci','sata','ide')]$oldBus = 'scsi',
    [Parameter(HelpMessage='New vmDisk bus type')]
        [ValidateSet('scsi','pci','sata','ide')]$newBus = 'pci',
    [Parameter(HelpMessage='VM disk bus index as next-available')]$indexOverride = $false,
    [Parameter(HelpMessage='User Confirmation Required')]$Confirm = $true,
    [Parameter(HelpMessage='Only modify one specific vmDisk')]$vmDiskId = $null
)

<#
.SYNOPSIS
    Change NTNX AHV VM virtual disk storage controller bus type.

.DESCRIPTION
    Script will take existing NTNX AHV VM virtual disks of one controller bus type, 
    clone them to new virtual disks of a different controller bus type with the same device index,
    and finally cleanup, removing original virtual disks.

    This operation requires that the VM is in a powered off state.

    Ensure that the VM Guest OS has appropriate drivers preinstalled or it will not boot after change.
    Linux systems may need special care in /etc/fstab to ensure block devices mount properly.
        1. Ensure mounts are based on blkid. (best)
        2. Or if needed pre create fstab entries for expected new block device names before pre-change poweroff.
            scsi / sata = sda...
            ide = hda...
            pci = vda...

.NOTES
    Author: Aaron Meyer

.PARAMETER NtnxCluster (Required)
    Nutanix Cluster Prism Element (PE) instance FQDN managing VM.

.PARAMETER vmName (Required)
    Name of the VM to action on as displayed in the Prism VM inventory.

.PARAMETER Cred
    PowerShell credential object containing access credentials for Prism Element.

.PARAMETER oldBus
    VM disk storage controller type to change FROM. Default=scsi
    Options: scsi, ide, sata, pci

.PARAMETER newBus
    VM disk storage controller type to change TO, Default=pci
    Options: scsi, ide, sata, pci

.PARAMETER vmDiskId
    Default behavior of the script will change ALL drives of oldBus type to newBus type.
    If you only want to modify a singular vmDisk use this parameter to specify the disk.
    Useful if you only wish to change one of many vmDisks.
    Noted in 'bustype'-'index'
    Options: scsi-0, sata-1, ide-3, etc.
    example: -vmDiskId scsi-0

.PARAMETER indexOverride
    Allocate VM disk bus inded as next-available instead of default which will match the source disk index.
        i.e. scsi-0 could become pci-2 (sda to vdc vs. vda)

    Most likely used when chaning to IDE bus type and CD-ROM owns ide-0.
    Alternative, remove CD-ROM, make change, re-add CD-ROM.
    
    WARNING: This may cause Guest OS to not properly mount block devices, UEFI boot options, etc.
    Use with caution. Ensure Linux fstab entires are referencing blkid and not device name.
    Have OS recovery media avialable in case manual intervention in guest OS is required.

.PARAMETER Confirm
    If $True (default) require user confirmation before making changes.

.EXAMPLE
    Change VM 'MyVMName' from scsi to pci storage bus. Script will prompt for credentials.
    & '.\NTNX_ChangeDiskBusType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'MyVMName'

    Change VM 'TestVM2' from scsi to sata storage bus using already defined credentials.
    & '.\NTNX_ChangeDiskBusType.ps1 -NtnxCluster 'PrismElement.example.com' -vmName 'TestVM2' -Cred $MyCreds -newBus 'sata'
#>

$LoadedSnapins = Get-PSSnapin -Name NutanixCmdletsPSSnapin -ErrorAction SilentlyContinue
if (-not $LoadedSnapins) {   
    Try{
        Add-PsSnapin NutanixCmdletsPSSnapin -ErrorAction Stop
        }
    Catch{
        Write-Host 'Nutanix PSSnapin could not be loaded'
        Write-Host 'Ensure Nutanis PowerShell Cmdlets is installed'
        Exit 1
    }
}

# Get creds if we weren't provided any.
If (! $Cred){
    $Cred = Get-Credential -Message "Admin credentials for NTNX Cluster $NtnxCluster"
}

Write-Host "Connecting to NTNX Cluster: $NtnxCluster... " -NoNewline
$NtnxConnection = Connect-NTNXCluster -Server $NtnxCluster -UserName $Cred.UserName -Password $Cred.Password -AcceptInvalidSSLCerts -ForcedConnection
If (! $NtnxConnection.IsConnected){
    Write-Host -ForegroundColor:Yellow ("FAILED`nUnable to connect to NTNX Cluster "+$NtnxCluster)
    Write-Host -ForegroundColor:Yellow $Error
    Exit 1
} Else{
    Write-Host -ForegroundColor:Green "SUCCESS"
}

# Get VM Info
$vmInfo = $null
$vmInfo = Get-NTNXVM | Where-Object {$_.vmName -eq $vmName}
If (! $vmInfo){
    Write-Host -ForegroundColor:Yellow 'Unable to find VM with specified name.'
    Exit 1
} ElseIf ($vmInfo.Count -gt 1){
    Write-Host -ForegroundColor:Yellow 'Multiple VMs returned! Do not use wildcard and ensure vmName is unique in cluster!'
    Exit 1
} ElseIf ($vmInfo.powerState -ne 'off'){
    Write-Host -ForegroundColor:Yellow 'VM was found but is currently powered on. Power off VM and try again.'
    Exit 1
} Else {
    Write-Host -ForegroundColor:Green 'Located VM and verified powered off state.'
}

# Get existing disks on old bus type, excluding CD-ROM
$oldDisks = $null
If ($vmDiskId -ne $null){
    Write-Host -ForegroundColor:Yellow ('Only modifying: '+$vmDiskId)
    ($bus, $index) = $vmDiskId.Split('-')
    $oldDisks = Get-NTNXVMDisk -Vmid $vmInfo.vmId -BusType:$bus -DeviceIndex:$index | Where-Object {$_.isCdrom -eq $false}
} Else {
    $oldDisks = Get-NTNXVMDisk -Vmid $vmInfo.vmId -BusType:$oldBus | Where-Object {$_.isCdrom -eq $false}
}

If ($oldDisks.Count -eq 0){
    Write-Host -ForegroundColor:Yellow ('No virtual disks were found on this VM of bus type: '+$oldBus)
    Exit 0
}

# Get all attached virtual disks
$allDisks = $null
$allDisks = Get-NTNXVMDisk -Vmid $vmInfo.vmId 

# Enforce new bus constraints
Switch ($newBus){
    'ide' {
        Write-Host ('You are trying to convert '+$oldDisks.Count+' disks to IDE')
        If ($oldDisks.Count -gt 4){
            Write-Host -ForegroundColor:Red ('IDE Bus supports maximum of 4 disks')
            Write-Host -ForegroundColor:Red ('One of which SHOULD be a CD-ROM')
            Exit 1
        } ElseIf ($oldDisks.Count -eq 4){
            Write-Host -ForegroundColor:Yellow ('IDE Bus only supports 4 disks, you won''t have a CD-ROM!')
        }
    }
    'sata' {
        Write-Host ('You are trying to convert '+$oldDisks.Count+' disks to SATA')        
        If ($oldDisks.Count -gt 6){
            Write-Host -ForegroundColor:Red ('SATA Bus supports maximum of 6 disks.')
            Exit 1
        }
    }
    'pci' {
        Write-Host ('You are trying to convert '+$oldDisks.Count+' disks to PCI')
    }
    'scsi' {
        Write-Host ('You are trying to convert '+$oldDisks.Count+' disks to SCSI')
    }
}

# Ensure there won't be a device Id conflict, else exit.
Write-Host 'Check for target bus id conflicts:'
$newIds = $oldDisks | ForEach-Object {($newBus+'-'+($_.id -split('-'))[1])}
$newIdConflict = $false
Foreach ($newId in $newIds){
    If ($newId -in $allDisks.id){
        $newIdConflict = $true
        Write-Host -ForegroundColor:Yellow ("`tVM disk change ID conflict found on: "+$newId)
        Write-Host -ForegroundColor:Yellow ("`tRemove $newBus conflict on $newId and try again, or make change manually")
    } Else {
        Write-Host -ForegroundColor:green ("`tNo conflict on: $newId")
    }
}
If ($newIdConflict -and ! $indexOverride){ 
    Write-Host -ForegroundColor:Red 'Exiting due device conflicts.'
    Exit 1
} ElseIf ($newIdConflict -and $indexOverride){
    Write-Host -ForegroundColor:Yellow 'WARNING! Device conflicts found, but -indexOverride set so continuing anyway.'
} Else {
    Write-Host -ForegroundColor:Green 'No device conficts found.'
}

# Confirm with user before making changes.
If ($Confirm){
    Write-Host "Do you approve / confirm making changes? (Y/N):" -NoNewline
    Do {
        $in = Read-Host
    } Until ($in -iin ('Y','N'))
    If ($in -ieq 'N'){ Exit }
}

# Itterate over all VM vDisks converting SCSI bus attached to PCI bus attached.
Foreach ($oldDisk in $oldDisks){
    # Clear any existing error history
    $Error.Clear()
    Try {
        # Define config specs / config templates
        $vmDiskCloneSpec = New-NTNXObject -Name VMDiskSpecCloneDTO
        $vmDiskAddress = New-NTNXObject -Name VMDiskAddressDTO
        $newDisk = New-NTNXObject -Name VMDiskDTO
        
        # Create nested configuration object with values
        $vmDiskCloneSpec.vmDiskUuid = $oldDisk.vmDiskUuid
        $vmDiskAddress.deviceBus = $newBus
        If ($indexOverride -eq $false){
            $vmDiskAddress.deviceIndex = $oldDisk.id.Split('-')[1]
        }
        $newDisk.vmDiskClone = $vmDiskCloneSpec
        $newDisk.diskAddress = $vmDiskAddress
        If ($indexOverride){
            $newDiskId = ($newBus+'-*')
        } Else {
            $newDiskId = ($newBus+'-'+$oldDisk.Id.Split('-')[1])
        }

        # Add cloned disk to VM
        Write-Host ('Processing VM disk '+$oldDisk.id)
        Write-Host ("`tCreating new cloned disk "+$newDiskId+' from '+$oldDisk.Id+':') -NoNewline
        $taskAdd = Add-NTNXVMDisk -Vmid $vmInfo.vmId -Disks $newDisk -ErrorAction:Stop
        $taskAddStatus = $null
        $jobStatusCounter = 0
        Do {
            If ($jobStatusCounter -gt 0){ Start-Sleep -Seconds 1}
            $jobStatusCounter++
            $taskAddStatus = Get-NTNXTask -Taskid $taskAdd.taskUuid
        } Until ($taskAddStatus.progressStatus -in ('Failed','Succeeded'))
        Switch ($taskAddStatus.progressStatus){
            'Failed' {
                Write-Host -ForegroundColor:Red ("FAILED`n`tUnable to add new cloned disk "+$newDisk.id+' manual remediation required.')
            }
            'Succeeded' {
                Write-Host -ForegroundColor:Green 'SUCCESS'
                $verifyDisk = $false
                $verifyDisk = Get-NTNXVMDisk -Vmid $vmInfo.vmId -BusType $newBus | Where-Object {$_.sourceVmDiskUuid -eq $oldDisk.vmDiskUuid}
                If (! $verifyDisk){ 
                    Write-Host -ForegroundColor:Red ("`tUnable to verify creation of new disk via sourceVmDiskUuid mapping.`n"+$verifyDisk)
                } Else {
                    Write-Host -ForegroundColor:Green ("`tVerfied cloned disk via sourceVmDiskUuid mapping on "+$verifyDisk.Id)
                }
            }
        }        

    } Catch {
        Write-Host -ForegroundColor:Red 'Error thrown when adding cloned disk to VM.'
    } Finally {
        # ONLY run the following if there was no error adding the new cloned disk!
        # If the new disk was not created, then removing this one will permanently destroy the vmDisk.
        If (!$Error -and ($taskAddStatus.progressStatus -eq 'Succeeded') -and $verifyDisk.Id){
            Write-Host ("`tRemoving old disk "+$oldDisk.id+':') -NoNewline
            $taskRemove = Remove-NTNXVMDisk -Vmid $vmInfo.vmId -Diskaddress $oldDisk.id -ErrorAction:Stop
            $taskRemoveStatus = $null
            $jobStatusCounter = 0
            Do {
                if ($jobStatusCounter -ge 1){ Start-Sleep -Seconds 1 }
                $jobStatusCounter++
                $taskRemoveStatus = Get-NTNXTask -Taskid $taskRemove.taskUuid
            } Until ($taskRemoveStatus.progressStatus -in ('Failed','Succeeded'))
            Switch ($taskRemoveStatus.progressStatus){
                'Failed' {
                    Write-Host -ForegroundColor:Red ("FAILED`n`tUnable to remove source disk "+$oldDisk.id+' manual remediation required.')
                }
                'Succeeded' {
                    Write-Host -ForegroundColor:Green ('SUCCESS')
                }
            }                
        } Else {
            Write-Host -ForegroundColor:Yellow 'New clone disk not properly added or errors returned:'
            Write-Host -ForegroundColor:Yellow 'Manually review state and remediate!'
            Write-Host -ForegroundColor:Yellow $Error
        }
    }
}

